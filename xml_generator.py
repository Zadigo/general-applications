from xml.etree.ElementTree import Element, SubElement, tostring
import datetime

class CreateXML:
    """
    Create an Indeed XML string for the API

    Required fields ar
    """
    def __init__(self, **kwargs):

        self.source = source = Element('source')

        try:
            # Publisher
            publisher = SubElement(source, 'publisher')
            publisher.text = kwargs['publisher']

            # Publisher URL
            publisherurl = SubElement(source, 'publisherurl')
            publisherurl.text = kwargs['publisherurl']

            # Last build date
            lastbuilddate = SubElement(source, 'lastbuilddate')
            lastbuilddate.text = str(datetime.datetime.today())

            # Job
            job = SubElement(source, 'job')

            # Job title
            title = SubElement(job, 'title')
            title.text = self.create_cdata_title(kwargs['title'])

            # Date
            date = SubElement(job, 'date')
            date.text = self.create_cdata_title(kwargs['date'])

            # Reference number
            referencenumber = SubElement(job, 'referencenumber')
            referencenumber.text = self.create_cdata_title(kwargs['referencenumber'])

            # URL
            url = SubElement(job, 'url')
            url.text = self.create_cdata_title(kwargs['url'])

            # Company
            company = SubElement(job, 'company')
            company.text = self.create_cdata_title(kwargs['company'])

            # Source name
            sourcename = SubElement(job, 'sourcename')
            sourcename.text = self.create_cdata_title(kwargs['sourcename'])

            # City
            city = SubElement(job, 'city')
            city.text = self.create_cdata_title(kwargs['city'])

            # Region
            region = SubElement(job, 'region')
            region.text = self.create_cdata_title(kwargs['region'])

            # Country
            country = SubElement(job, 'country')
            country.text = self.create_cdata_title(kwargs['country'])

            # Postal code
            postalcode = SubElement(job, 'postalcode')
            postalcode.text = self.create_cdata_title(kwargs['postalcode'])

            # Email
            email = SubElement(job, 'email')
            email.text = '<![CDATA[email]]>'

            # Description
            description = SubElement(job, 'description')
            description.text = '<![CDATA[description]]>'

            # Education
            education = SubElement(job, 'education')
            education.text = '<![CDATA[education]]>'

            # Job type
            jobtype = SubElement(job, 'jobtype')
            jobtype.text = '<![CDATA[jobtype]]>'

            # Category
            category = SubElement(job, 'category')
            category.text = '<![CDATA[category]]>'

            # Experience
            experience = SubElement(job, 'experience')
            experience.text = '<![CDATA[experience]]>'

        except KeyError as e:
            print("Could not get the required key '%s'" % e.args)
            return None

    @staticmethod
    def create_cdata(text):
        return '<![CDATA[%s]]>' % text

    @staticmethod
    def create_cdata_title(text, title_category='h3'):
        return '<![CDATA[<{title_category}>{text}</{title_category}>]]>'.format(
            title_category=title_category,
            text=text
        )

    @property
    def get_xml_string(self):
        return tostring(self.source)

start = CreateXML(
    publisher='Kurrikulam', 
    publisherurl='http://www.kurrikulam/recruteurs',
    title='Chargé de marketing',
    date='2018-12-15',
    referencenumber=1,
    url='http://www.kurrikulam/offres/1',
    company='company',
    sourcename='sourcename',
    city='city',
    region='region',
    country='France',
    postalcode='59120',
    email='test@test.io',
    description='description',
    education='education',
    jobtype='jobtype',
    category='category',
    experience='experience'
)
print(start.get_xml_string)

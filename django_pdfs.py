from reportlab.pdfgen import canvas
from io import BytesIO

class PDFGenerator(View):
    """
    Creates a PDF that is automatically
    downloaded once created
    """
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'
        # response['Content-Disposition'] = 'filename="somefilename.pdf"'

        buffer = BytesIO()

        # Create the PDF object, using the response object as its "file."
        p = canvas.Canvas(buffer)

        # Put things on the PDF
        p.drawString(100, 100, "Hello world.")

        # Close & save
        p.showPage()
        p.save()

        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response


# View when using render_to_pdf``
# in order to force download`
class DownloadPDFView(View):
    def get(self, request, *args, **kwargs):
        pdf = render_to_pdf('test_pdf.html')
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "Invoice_%s.pdf" %("12341231")
            content = "inline; filename='%s'" %(filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" %(filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")
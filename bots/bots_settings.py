import os

# Le nom du fichier Google sheet à utiliser
NOM_FICHIER = 'Hotellerie'

# La feuille de calcul qui doit être utilisé
# IMPORTANT : doit être un chiffre qui correspond
# à la position de cette feuille
INDEX_FEUILLE_DE_CALCUL = 3

# Cela correspond à la plage de cellule
# où se trouve les URLs
PLAGE_DE_CELLULES_URL = 'B8:B200'

# Cela correspond à la plage de cellules
# où les emails doivent être écritsÒ
PLAGE_DE_CELLULES_EMAIL = 'D8:D200'


# NE PAS MODIFIER

BASE_PATH = os.path.abspath(os.path.dirname(__file__))

EMAIL_TXT_PATH = os.path.join(BASE_PATH, 'email.txt')

CREDENTIAL_FILE = os.path.join( BASE_PATH, 'talent_secret.json')

SCOPE = [
    'https://spreadsheets.google.com/feeds',
    'https://www.googleapis.com/auth/drive'
]

class GetCredentialFile:
    def __init__(self, nom_du_fichier=None):
        exists = []
        secret_files = ['secret', 'talent_secret', 'credentials']

        if nom_du_fichier:
            secret_files.append(nom_du_fichier)
        
        for secret_file in secret_files:
            path = os.path.join(BASE_PATH, secret_file + '.json')
            secret_exists = os.path.exists(path)
            exists.append(list((path, secret_exists)))

        for element in exists:
            if element[1] is True:
                CREDENTIAL_FILE = element[0]
                quit

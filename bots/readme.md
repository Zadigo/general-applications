# Utilisation

Pour utiliser le l'extracteur d'adresse mail, il suffit d'initier la class en faisant : `BeautifyHTML().parse_responses()`.

La class présente dans `bots.py` va récupérer les adresses mails dans le Google sheet puis, envoyer des requêtes à chaque urls. Une liste de réponse sera généré `[<response>, <response>, ...]` sur lesquelles nous allons itérer pour extraire le code HTML. Ce code ne comporte que les tags suivants : __<p>, <span> et <a>__ où se trouve généralement les adresses mails.

Le code HTML transformé en version text sera REGEX'er pour en extraire le mail en question. Le regex de base est : `\w.*\@\w+\.\w+` où __\w.*__ récupère tout les éléments qui se trouve devant le @ de l'adresse mail (example : __google_you@google.com__ / __google_you__ sera récupérer), __\@__ récupère le '@', __\w+__ récupère le domaine (example : __google__), __\.__ le point et finalement __\w+__ l'extension qui suit directement le domaine (example : __com__).

# MODIFICATONS

Pour modifier le fichier c'est très simple ! Il suffit juste de se rendre dans le fichier __bots_settings.py__ et d'ajuster les paramètres relatifs à la feuille de calcul ou encore au fichier que vous souhaitez utiliser.

# AUTORISATIONS

Pour se connecter au Drive, Google exige une série d'authentification. Si vous ne souhaitez utiliser autre que dans le Drive par défaut, vous devez vous rendre dans __Google API Console__ à cette adresse https://console.cloud.google.com/apis/dashboard?project=talentview-211113&folder&organizationId=1017083669378 et rechercher le terme 'sheets' dans la barre de recherche.

Après avoir choisi le lien correspondant, faites __Enable__ et dès lors vous pourrez créer un fichier que vous devrez télécharger et copier dans le dossier __bots.__

Renommez le fichier __talent_secret.json.__

Si vous ne souhaitez pas renommer le fichier et l'utiliser quand même, il vous suffit juste de mettre `GetCredentialFile(nom_de_votre_fichier)` juste avant `BeautifyHTML().parse_responses()`.
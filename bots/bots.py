import re
import time
import requests
from urllib.parse import urlparse, urlsplit
from sheet import GoogleSheetConnector
from bots_settings import EMAIL_TXT_PATH
from bs4 import BeautifulSoup
from random import choice
from sheet import GoogleSheetConnector
from bots_settings import GetCredentialFile

class BaseManager:
    """
    Liste des headers a utilisé pour contrer les robots
    et éviter que l'adresse IP ne se fasse éjecter du
    site internet
    """
    agents = [
        "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0",
        "Mozilla/5.0 (Android 8.0; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0",
        "Mozilla/5.0 (Android 8.0; Tablet; rv:41.0) Gecko/41.0 Firefox/41.0",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:10.0) Gecko/20100101 Firefox/59.0",
        "Mozilla/5.0 (iPod touch; CPU iPhone OS 8_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) FxiOS/1.0 Mobile/12F69 Safari/600.1.4",
        "Mozilla/5.0 (iPhone; CPU iPhone OS 8_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) FxiOS/1.0 Mobile/12F69 Safari/600.1.4",
        "Mozilla/5.0 (iPad; CPU iPhone OS 8_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) FxiOS/1.0 Mobile/12F69 Safari/600.1.4"
    ]

class ScrappingBot(BaseManager):
    urls = GoogleSheetConnector()
    responses = []

    def __init__(self):
        try:
            # On génère un agent automatiquement pour
            # ne pas se faire attrapper par les bots
            print('\n')
            print('Envois de(s) (la) requête(s)...')
            print('\n')

            t = 0

            for url in self.urls.get_urls():
                t += 1
                print('%s : %s' % (t, url))
                self.responses.append(requests.get(url, choice(self.agents)))
                time.sleep(1)

        except requests.ConnectionError as e:
            print("Il y a eu une erreur de connexion : %s" % e.args)
            
        else:
            pass

class BeautifyHTML(ScrappingBot):
    def parse_responses(self):
        print('\n')
        print('Récupération des tags <p>, <a> et <span>...')
        print('-'*20)

        for response in self.responses:
            soup = BeautifulSoup(response.text, 'html.parser')
            tags = soup.find_all(['p', 'span', 'a'])

            for tag in tags:
                # On enlève l'espace blanc tout autour
                # du tag avant le REGEX
                stripped_tag = str(tag.text).strip()

                # REGEX
                email = re.match(r'\w.*\@\w+\.\w+', stripped_tag)

                if email:
                    print("Écriture de l'adresse mail : %s" % email.group(0))

                    with open(EMAIL_TXT_PATH, mode='a', encoding='utf-8') as f:
                        f.writelines(email.group(0))
                        f.writelines('\n')

                    self.urls.write_emails(email)

        print('\n')
        print('- Terminé')


# Execution
BeautifyHTML().parse_responses()
import os
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from bots_settings import NOM_FICHIER, INDEX_FEUILLE_DE_CALCUL, PLAGE_DE_CELLULES_URL, PLAGE_DE_CELLULES_EMAIL, CREDENTIAL_FILE, SCOPE 

class GoogleSheetConnector:
    """
    Cette fonction permet de se connecter au
    fichier Google Sheets des adresses pour les récupérer pour le bot
    """
    def __init__(self):
        print('-'*20)
        print('Connection à Google Sheet...', end="\n")
        print('Fichier : %s' % NOM_FICHIER)

        credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIAL_FILE, SCOPE)
        client = gspread.authorize(credentials)

        try:
            self.spreadsheet = client.open(NOM_FICHIER)
            
        except gspread.exceptions.SpreadsheetNotFound:
            print("Le fiichier '%s' n'existe pas dans votre drive. Avez-vous utiliser le nom exact ?" % NOM_FICHIER)
            raise

    @property
    def get_spread_sheet(self):
        return self.spreadsheet

    def get_urls(self):
        """
        Retourne la feuille de calcul qui contient
        les adresses à analyser : [url1, url2, ...]
        """
        output = self.spreadsheet.get_worksheet(INDEX_FEUILLE_DE_CALCUL)
        return [cell.value for cell in output.range(PLAGE_DE_CELLULES_URL) if cell.value != '']

    def write_emails(self, value):
        wk = self.spreadsheet.get_worksheet(INDEX_FEUILLE_DE_CALCUL)
        output_range = wk.range(PLAGE_DE_CELLULES_EMAIL)
        
        for cell in output_range:
            cell.value = value

        print('----- Terminé -----')

GoogleSheetConnector().write_emails('This is a test')

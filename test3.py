from collections import namedtuple

class Descriptor:
    def __init__(self, ville, pays):
        self.ville = ville
        self.pays = pays

    def __setattr__(self, name, value):
        return super().__setattr__(name, value)

class Typed(Descriptor):
    ville = None
    def __setattr__(self, name, value):
        if value == 'Paris':
            print('Vous êtes Parisien')
        super().__setattr__(name, value)

class Ville(Typed):
    ville = None

class France(Descriptor):
    pays = None
    def __setattr__(self, name, value):
        if self.pays != 'France':
            print('Vous êtes étranger')
        super().__setattr__(name, value)
    
class Francais(Ville, France):
    pass


class A(type):
    def __new__(cls, name, bases, clsdict):
        new_class = super().__new__(cls, name, bases, clsdict)
        otacos = namedtuple('Otacos', ['zone', 'distance'])
        setattr(new_class, 'otacos', otacos)
        print(clsdict)
        return new_class

class B(metaclass=A):
    _fields = []

class W(B):
    # zone = Ville('Paris', 'France')
    pays = Francais('Paris', 'France')

s = W()
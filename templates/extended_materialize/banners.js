$(document).ready(function () {
    $('.banner').on('click', function() {
        var name = $(this).data('name');
       $(this).parent().children('div.banner').each(function(e) {
           $(this).removeClass('selected');
       });
        $.ajax({
            type: 'POST',
            url: 'https://www.example.com/',
            data: {'a': name},
            dataType: 'json',
            success: function(response) {
                alert(response);
            }
        });
        $(this).addClass('selected');
    });
});
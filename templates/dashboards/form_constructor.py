class FormConstructor:
    def __init__(self, attrs=()):
        html = ''

        for attr in attrs:
            for i in range(0, len(attr)):
                input_type = f"""
                    <div class="input-field col s{attr[0]} l{attr[1]} m{attr[2]}"
                        <input type="{attr[3]}" name="{attr[4]}" id="{attr[5]}" placeholder="{attr[6]}">
                    </div>
                """
            html += input_type.strip()
        self.html=html

    @property
    def get_inputs(self):
        return self.html
s=(
    (12,6,6,'text', 'text','text','Votretext...'),
    (12,12,6,'search', 'search','search','Votretext...')
)
print(FormConstructor(s).get_inputs)

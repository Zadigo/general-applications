$(document).ready(function() {
    (function() {
        var table = $('table');
        var table_rows = table.find('tr');
        
        var checkbox_tag = table.find('.checkbox-tag');
        var checkbox = checkbox_tag.find('input[type="checkbox"]');
        var text_tag = table.find('.text-tag');

        function checkboxIsChecked() {
            return checkbox.is(':checked');
        }

        var selectFieldsHover = function() {
            text_tag.parent().on('mouseenter', function() {
                text_tag.addClass('hide');
                checkbox_tag.removeClass('hide').fadeIn('slow');
            }).on('mouseleave', function() {
                if (checkboxIsChecked()) {
                    // pass
                } else {
                    text_tag.removeClass('hide').fadeIn('slow');
                    checkbox_tag.addClass('hide');
                }
            })
        }

        var dataSelection = function() {
            checkbox.on('click', function(e) {
                if ($(this).is(':checked')) {
                    table.find('tr td input[type="checkbox"]').each(function() {
                        $(this).prop({'checked': true});
                    });
                };
            })
        }

        selectFieldsHover();
        dataSelection();
    })();

    (function() {
        $('.navbar a').on('click', function() {
            var target = $(this).data('target');
            $('#' + target).toggleClass('show');
        })
    })();

    // (function() {
    //     $('#night-mode').find('input[type="checkbox"]').on('click', function() {
    //         $('.sidenav').css({'background-color': 'black', 'color': 'white'});
    //     })
    // })();

    $('.collapsible').collapsible();
    $('.datepicker').datepicker({ format: 'mm-dd-yyyy'});
    $('select').formSelect(); 
    M.updateTextFields();
})
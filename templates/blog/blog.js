$(document).ready(function() {
    // This adds an event on the social buttons
    // formatted as 'btn-twitter' for example
    // and contained in a 'social' div
    $('.btn').on('mouseover', function() {
        var w = $(this).attr('class');

        // Checks if social buttons 
        // are in a social div
        var a = /social/g;
        // Checks if button is a 
        // social button
        var p = /btn\-([a-z]+)/g;

        var e = $(this).parent().attr('class');
        var b = a.exec(e);

        if (b) {
            var is_social = p.exec(w);
            if (is_social) {
                var m = is_social[1];
                var t = ['twitter', 'facebook'].includes(m);
                if (t) {
                    // DO SOMETHING
                }
            };
        };
    });

    // Footer plugin
    (function( $ ) {
 
        $.fn.sticky = function() {
            this.addClass('sticky');
        };
     
    }( jQuery ));
});
from io import BytesIO
from xhtml2pdf import pisa

from django.template.loader import get_template
from django.http import HttpResponse

from cgi import escape

import string

import datetime

import hashlib

from random import choice

from urllib.parse import urlencode, urljoin



def render_to_pdf(template, template_dict={'pagesize':'A4'}):
    """
    This defintion creates a PDF from an HTML template
    """
    tpl = get_template(template)
    html = tpl.render(template_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return HttpResponse('There was an error')


def inverse_transform_date(d):
    """
    Use this module to transform-inverse
    a date from ``20 Septembre 2018`` to
    ``2018-09-20`` for various url or
    database purposes.

    Month should be in french.

    """
    # t = '1 Septembre 2018'
    elements = d.split(' ')
    # a = datetime.now()
    # print(datetime.strftime(a, '%Y-%m-%d'))
    # print(datetime.strftime(a, '%d-%m-%Y'))
    mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin',
        'juillet', 'Août', 'septembre', 'octobre', 'novembre', 'décembre']
    new_date_array = []
    for element in elements:
        if element in mois:
            # Get the position of the month in
            # the list and add one to get the
            # real world month
            pos = str(mois.index(element) + 1)
            new_date_array.append(pos)
        else:
            new_date_array.append(element)
    # Inverse the array
    # e.g. 20 09 2018 ->
    # 2019 09 20
    new_date_array.reverse()
    new_date = '-'.join(new_date_array)
    return datetime.strptime(new_date, '%Y-%m-%d')

# def get_current_time():
#     return datetime.strftime(datetime.now(), '%H:%m')

# def random_token_creator(string=string.ascii_uppercase+string.ascii_lowercase):
#     return ''.join(_ for _ in choice(string))

def create_hash(string):
    return hashlib.md5(string.encode('utf-8')).hexdigest()

class ContextCreator:
    def __init__(self):
        self.context = {}

    def _context(self, *args, **kwargs):
        """
        Return a context element from various
        things from Django
        """
        if args:
            for arg in args:
                self.context[arg] = arg

        if kwargs:
            for key, value in kwargs.items():
                if isinstance(value, list):
                    self._parser(value)
                elif isinstance(value, dict):
                    self.context.update(value)
                else:
                    self.context[key] = value
        return self.context

    def form_context(self, form_id, form_object, form_button, method='GET', **kwargs):
        """
        Create a context specific for a form 
        by using this method.

        You can pass other special parameters
        such as `action`.

        """
        if not isinstance(form_object, type):
            raise TypeError('Form object should be of type class.'
                    ' Received %s' % form_object.__class__.__name__)

        self.context['id'] = form_id
        self.context['method'] = method
        self.context['form'] = form_object
        self.context['form_button'] = form_button
        return self.context

    def _parser(self, values):
        for value in values:
            self.context.update({value:value})


from collections import Counter

class SEOTools:
    def text_length(self, text):
        minimum_length = 300
        text_length = len(text)
        if text_length >= minimum_length:
            return True
        else:
            return False

    def text_truncate(self, text, size):
        return text[:size]

    def keyword_density(self, text, keyword):
        words = text.split(' ')
        number_of_words = len(words)
        count_of_keyword = Counter(words)
        return count_of_keyword.get(keyword) / number_of_words

# print(campaign_builder('http://www.kurrikulam.com/', 'facebook', 'social', 'signup'))

import datetime
import calendar
def create_timestamp(d):
    return datetime.datetime.strptime(d, '%Y-%m-%d').timestamp()

def create_timestamp_two():
    e = datetime.datetime.date(datetime.datetime.now())
    p = e.strftime('%s')
    print(p)

def create_timestamp_three():
    e = datetime.datetime.date(datetime.datetime.now())
    p = calendar.timegm(e.timetuple())
    print(p)

create_timestamp_three()
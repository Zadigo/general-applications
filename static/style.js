$(document).ready(function() {
    // Replace each space with a
    // dash e.g. this-is-a-text
    $('.text').on('keyup', function() {
        var w = $(this).val().replace(/[\s]+/g, '-');
        $('.r').text(w); 
    });

    // Count the amount of words
    $('.text').on('keyup', function() {
        var words = $('.text').val().split(' ');
       $('.result').text(words.length);
    });

    // Count the amount of characters
    $('.text').on('keyup', function() {
        var words = $('.text').val();
       $('.result').text(words.length);
    });

    // Get length of text
    // dynamically
    $('.text').on('keyup', function() {
        var words = $('.text').val();
        var l = words.length;
        $('.result').text(l);
        if (l > 300) {
            $('span.percent').css('background-color', 'red');
        } else {
            $('span.percent').css('background-color', 'white');
        }
    });

    // Get density of a word in a
    // text dynamically

    // Create UTM links
    var pattern = /https?\:\/\/www\.[a-zA-Z0-9]+\.\w+\/?/g
    $('.link').on('keyup', function() {
        var link = $(this).val();
        $('.output').text(link);
    });
    $('.utm_source').on('click', function() {
        $(this).on('keyup', function() {
            var new_link = $('.output').val() + '/utm_source=' + $(this).val();
            $('.output').text(new_link);
        })
    });
    $('.utm_medium').on('click', function() {
        $(this).on('keyup', function() {
            var new_link = $('.output').val() + '/utm_medium=' + $(this).val();
            $('.output').text(new_link);
        })
    })
});
import datetime
import re

LANGUES = (
    ('anglais', 'Anglais'),
    ('espagnol', 'Espagnol'),
)

NIVEAU_LANGUE = (
    ('b1', 'B1'),
)

COURSE_PACKS = (
    ('starter', 'Starter'),
    ('starter plus', 'Starter plus'),
)

STATUT_CANDIDATURE = (
    ('standby', 'Standby'),
    ('proposition', 'Proposition'),
    ('meeting', 'Meeting'),
    ('refusé', 'Refusé'),
    ('accepté', 'Accepté'),
    ('holiday', 'Holiday'),
)

STATUT_LEARNER = (
    ('etudiant', 'Etudiant'),
    ('salarie', 'Salarie'),
)

MATERIAL_ICONS = (
    ('check_circle', 'check_circle'),
    ('done', 'done'),
    ('favorite', 'favorite'),
    ('map', 'map'),
    ('thumb_up', 'thumb_up'),
    ('local_offer', 'local_offer'),
    ('person', 'person'),
    ('place', 'place'),
    ('star', 'star'),
    ('euro_symbol', 'euro_symbol'),
    ('event', 'event'),
)

COUNTRIES = (
    ('albania', 'Albania'),
    ('algeria', 'Algeria'), 
    ('angola', 'Angola'),
    ('anguilla', 'Anguilla'),
    ('antigua and barbuda', 'Antigua and barbuda'),
    ('argentina', 'Argentina'),
    ('armenia', 'Armenia'),
    ('australia', 'Australia'),
    ('austria', 'Austria'),
    ('azerbaijan', 'Azerbaijan'),
    ('bahamas', 'Bahamas'),
    ('barbados', 'Barbados'),
    ('belarus', 'Belarus'),
    ('belgium', 'Belgium'),
    ('belize', 'Belize'),
    ('benin', 'Benin'),
    ('bermuda', 'Bermuda'),
    ('bolivia (plurinational state of)', 'Bolivia (plurinational state of)'),
    ('bosnia and herzegovina', 'Bosnia and Herzegovina'),
    ('botswana', 'Botswana'),
    ('brazil', 'Brazil'),
    ('british indian ocean territory', 'British Indian Ocean Territory'), 
    ('virgin islands (british)', 'Virgin islands (british)'), 
    ('virgin islands (u.s.)', 'Virgin islands (u.s.)'), 
    ('bulgaria', 'Bulgaria'), 
    ('burkina faso', 'Burkina faso'), 
    ('cambodia', 'Cambodia'), 
    ('cameroon', 'Cameroon'), 
    ('canada', 'Canada'), 
    ('caboverde', 'Cabo verde'), 
    ('cayman islands', 'Cayman islands'), 
    ('central african republic', 'Central African Republic'), 
    ('chad', 'Chad'), 
    ('chile', 'Chile'), 
    ('china', 'China'),
    ('colombia', 'Colombia'),
    ('comoros', 'Comoros'),
    ('congo', 'Congo'),
    ('congo (The democratic republic of the)', 'Congo (The democratic republic of the)'),
    ('costa rica', 'Costa rica'),
    ("côte d'ivoire", "Côte d'ivoire"),
    ('croatia', 'Croatia'),
    ('cuba', 'Cuba'),
    ('cyprus', 'Cyprus'),
    ('czech republic', 'Czech republic'),
    ('denmark', 'denmark'),
    ('djibouti', 'djibouti'),
    ('dominica', 'dominica'),
    ('dominican republic', 'dominican republic'),
    ('ecuador', 'Ecuador'),
    ('egypt', 'Egypt'),
    ('el salvador', 'El Salvador'),
    ('equatorial guinea', 'Equatorial Guinea'),
    ('estonia', 'Estonia'),
    ('ethiopia', 'Ethiopia'),
    ('finland', 'Finland'),
    ('france', 'France'),
    ('french guiana', 'French guiana'),
    ('french polynesia', 'French polynesia'),
    ('french southern territories', 'French Southern Territories'),
    ('gabon', 'Gabon'),
    ('gambia', 'Gambia'),
    ('georgia', 'Georgia'),
    ('germany', 'Germany'),
    ('ghana', 'Ghana'),
    ('gibraltar', 'Gibraltar'),
    ('greece', 'Greece'),
    ('greenland', 'Greenland'),
    ('grenada', 'Grenada'),
    ('guadeloupe', 'Guadeloupe'),
    ('guatemala', 'Guatemala'),
    ('guinea', 'Guinea'),
    ('guinea-bissau', 'Guinea-bissau'),
    ('guyana', 'Guyana'),
    ('haiti', 'Haiti'),
    ('honduras', 'Honduras'),
    ('hong kong', 'Hong Kong'),
    ('hungary', 'Hungary'),
    ('iceland', 'Iceland'),
    ('india', 'India'),
    ('indonesia', 'Indonesia'),
    ('iraq', 'Iraq'),
    ('ireland', 'Ireland'),
    ('israel', 'Israel'),
    ('italy', 'Italy'),
    ('jamaica', 'jamaica'),
    ('japan', 'japan'), 
    ('kazakhstan', 'Kazakhstan'),
    ('kenya', 'Kenya'),
    ('korea (republic of)', 'Korea (republic of)'),
    ('kosovo', 'Kosovo'),
    ('latvia', 'latvia'),
    ('liberia', 'liberia'),
    ('libya', 'libya'),
    ('lithuania', 'lithuania'),
    ('luxembourg', 'luxembourg'),
    ('macedonia (the former yugoslav republic of)', 'macedonia (the former yugoslav republic of)'),
    ('madagascar', 'madagascar'),
    ('malaysia', 'malaysia'),
    ('maldives', 'maldives'), 
    ('mali', 'mali'),
    ('malta', 'malta'),
    ('martinique', 'martinique'),
    ('mauritania', 'mauritania'),
    ('mauritius', 'mauritius'),
    ('mexico', 'mexico'),
    ('moldova (republic of)', 'moldova (republic of)'),
    ('monaco', 'monaco'),
    ('montenegro', 'montenegro'),
    ('morocco', 'morocco'),
    ('mozambique', 'mozambique'),
    ('namibia', 'Namibia'),
    ('nepal', 'Nepal'),
    ('netherlands', 'Netherlands'),
    ('new caledonia', 'New Caledonia'),
    ('new zealand', 'New Zealand'),
    ('nicaragua', 'Nicaragua'),
    ('niger', 'Niger'),
    ('nigeria', 'Nigeria'),
    ('norway', 'Norway'),
    ('pakistan', 'Pakistan'),
    ('panama', 'Panama'),
    ('paraguay', 'Paraguay'),
    ('peru', 'Peru'),
    ('philippines', 'Philippines'),
    ('poland', 'Poland'),
    ('portugal', 'Portugal'),
    ('puerto rico', 'Puerto rico'),
    ('qatar', 'qatar'),
    ('réunion', 'Réunion'),
    ('romania', 'Romania'),
    ('russian federation', 'Russian Federation'),
    ('saint barthélemy', 'Saint Barthélemy'),
    ('saint kitts and nevis', 'Saint kitts and Nevis'),
    ('saint lucia', 'Saint lucia'),
    ('saint martin', 'Saint Martin'),
    ('saudi arabia', 'Saudi Arabia'),
    ('senegal', 'Senegal'),
    ('serbia', 'Serbia'),
    ('seychelles', 'Seychelles'),
    ('singapore', 'Singapore'),
    ('slovakia', 'Slovakia'),
    ('slovenia', 'Slovenia'),
    ('south africa', 'South Africa'),
    ('spain', 'Spain'),
    ('suriname', 'Suriname'),
    ('swaziland', 'Swaziland'),
    ('sweden', 'Sweden'),
    ('switzerland', 'Switzerland'),
    ('taiwan', 'Taiwan'),
    ('thailand', 'Thailand'),
    ('togo', 'Togo'),
    ('trinidad and tobago', 'Trinidad and Tobago'),
    ('tunisia', 'Tunisia'),
    ('turkey', 'Turkey'),
    ('turkmenistan', 'Turkmenistan'),
    ('uganda', 'Uganda'),
    ('ukraine', 'Ukraine'),
    ('united arab emirates', 'United Arab Emirates'),
    ('united kingdom', 'United Kingdom'),
    ('united states', 'United States'),
    ('uruguay', 'uruguay'),
    ('uzbekistan', 'uzbekistan'),
    ('venezuela', 'Venezuela'),
    ('viet nam', 'viet nam'),
    ('zambia', 'zambia'),
    ('zimbabwe', 'zimbabwe'),
)

def credit_card_years():
    """
    Use this method to generate the choices for a
    credit card form from which the user can choose
    the year at which the card expires
    """
    pattern = r'20([0-9]{2})'
    current_year = datetime.datetime.today().year
    e = []
    for i in range(0, 10):
        r = str(current_year + i)
        p = re.search(pattern, str(r)).group(1)
        e.append((p, r))
    return tuple(e)

def credit_card_months():
    """
    Use this method to generate the choices for a
    credit card form from which the user can choose
    the month at which the card expires
    """
    months = ["Jan", "Feb", "Mar", "Apr", "May", "June", 
            "July", "Aug", "Sep", "Oct", "Nov", "Dec"]
    p = []
    for i in range(0, 12):
        # We have to put 8 to
        # compensate for the
        # iteration on the array
        if i <= 8:
            u = '0' + str(i + 1)
        else:
            u = str(i + 1)
        v = (str(u), months[i] +  ' (' + u + ')')
        p.append(v)
    return tuple(p)